#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 22 12:51:40 2023

@author: seba
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

cer_url = "https://www.datos.gob.ar/series/api/series/?ids=94.2_CD_D_0_0_10"
cer_csv_url = "https://infra.datos.gob.ar/catalog/sspm/dataset/94/distribution/94.2/download/cer-uva-uvi-diarios.csv"

cer_csv_file = "cer-uva-uvi-diarios.csv"

# %% load the table

data = pd.read_csv(cer_csv_file)

# %% decide if needed to download a new csv

# check the date

# if date is too old download the new file and reload the table

# %%

data.plot(x="indice_tiempo", y="cer_diario")

# %% cuenta rapida para salir del paso
dataarr = np.array(data)

t0, t1, t2, t3 = tt = ["2023-0%d-01"%i for i in range(2, 6)]
certt = np.array([dataarr[np.argwhere(dataarr[:, 0]==t)[0,0], 1] for t in tt])

print("inflacion neta", certt[-1] / certt[0])

mesmes = certt[1:] / certt[:-1]
print("inflacion mes a mes", mesmes)

sumamal = 1 + np.sum(mesmes - 1)
print("suma incorrecta mes a mes", sumamal)

# %% sacado directo de la pagina del indec IPC
# https://www.indec.gob.ar/indec/web/Institucional-Indec-InformesTecnicos-31

# marzo, abril y mayo
tresmeses = np.array([7.7, 8.4, 7.8])
print("tasa de inflacion relativa mensual", tresmeses)
sumamal = np.sum(tresmeses)
print("suma directa, cuenta mal", sumamal)

prodbien = 100 * (np.prod(1 + (tresmeses / 100)) - 1)
print("producto de tasa, bien hecho", prodbien)

# %% calculo teorico de cuanto difiere hace rla cuenta bien y mal

# tasa mensual
k = np.arange(1, 20)
n = 6 # meses

sumamal = k * n
prodbien = 100 * ((k / 100 + 1)**n - 1)

plt.clf()
plt.scatter(prodbien, sumamal)
plt.plot([1, prodbien[-1]], [1, prodbien[-1]])
plt.xlabel("tasa % calculada con el producto, bien")
plt.ylabel("tasa % calculada con la suma directa, mal")
plt.title("acumulacion de interes/inflacion a %d meses"%n)