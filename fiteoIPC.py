#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 13 17:56:55 2023

@author: sebalander
"""
import numpy as np
import matplotlib.pyplot as plt

meses = ["Dec-16", "Jan-17", "Feb-17", "Mar-17", "Apr-17", "May-17", "Jun-17", "Jul-17", "Aug-17", "Sep-17", "Oct-17", "Nov-17", "Dec-17", "Jan-18", "Feb-18", "Mar-18", "Apr-18", "May-18", "Jun-18", "Jul-18", "Aug-18", "Sep-18", "Oct-18", "Nov-18", "Dec-18", "Jan-19", "Feb-19", "Mar-19", "Apr-19", "May-19", "Jun-19", "Jul-19", "Aug-19", "Sep-19", "Oct-19", "Nov-19", "Dec-19", "Jan-20", "Feb-20", "Mar-20", "Apr-20", "May-20", "Jun-20", "Jul-20", "Aug-20", "Sep-20", "Oct-20", "Nov-20", "Dec-20", "Jan-21", "Feb-21", "Mar-21", "Apr-21", "May-21", "Jun-21", "Jul-21", "Aug-21", "Sep-21", "Oct-21", "Nov-21", "Dec-21", "Jan-22", "Feb-22", "Mar-22", "Apr-22", "May-22", "Jun-22", "Jul-22", "Aug-22", "Sep-22", "Oct-22", "Nov-22", "Dec-22", "Jan-23", "Feb-23", "Mar-23", "Apr-23"]

ipc = np.array([[100.0, 101.6, 103.7, 106.1, 109.0, 110.5, 111.8, 113.8, 115.4, 117.6, 119.4, 121.0, 124.8, 127.0, 130.1, 133.1, 136.8, 139.6, 144.8, 149.3, 155.1, 165.2, 174.1, 179.6, 184.3, 189.6, 196.8, 206.0, 213.1, 219.6, 225.5, 230.5, 239.6, 253.7, 262.1, 273.2, 283.4, 289.8, 295.7, 305.6, 310.1, 314.9, 322.0, 328.2, 337.1, 346.6, 359.7, 371.0, 385.9, 401.5, 415.9, 435.9, 453.7, 468.7, 483.6, 498.1, 510.4, 528.5, 547.1, 560.9, 582.5, 605.0, 633.4, 676.1, 716.9, 753.1, 793.0, 851.8, 911.1, 967.3, 1028.7, 1079.3, 1134.6, 1203.0, 1282.7, 1381.2, 1497.2],
[100.0, 101.3, 103.2, 106.0, 108.4, 109.8, 110.7, 112.0, 114.3, 116.4, 118.2, 119.6, 120.4, 122.9, 125.6, 128.5, 130.0, 134.2, 141.2, 146.8, 152.7, 163.4, 173.0, 178.9, 181.9, 188.0, 198.8, 210.6, 216.0, 221.3, 226.9, 232.0, 242.5, 256.4, 262.7, 276.6, 285.3, 298.7, 306.6, 318.7, 328.8, 331.0, 334.5, 338.8, 350.5, 360.9, 378.1, 388.4, 405.3, 424.8, 441.1, 461.3, 481.2, 496.2, 512.3, 529.7, 537.6, 553.4, 572.3, 584.5, 609.3, 639.4, 687.2, 736.9, 780.1, 814.6, 852.2, 903.5, 967.6, 1032.8, 1096.5, 1135.0, 1187.9, 1268.3, 1392.4, 1522.5, 1677.0],
[100.0, 100.9, 105.2, 107.3, 109.8, 111.7, 112.5, 115.8, 117.3, 118.1, 121.7, 123.0, 123.7, 126.6, 128.7, 129.6, 131.3, 133.4, 134.6, 138.1, 140.0, 146.2, 149.6, 156.5, 158.7, 164.1, 168.0, 174.8, 176.6, 180.5, 185.4, 186.9, 195.1, 206.2, 219.0, 231.2, 238.4, 248.7, 252.0, 259.2, 262.8, 263.0, 273.0, 276.7, 280.3, 292.3, 297.9, 306.7, 317.2, 331.4, 343.3, 365.4, 378.5, 384.6, 405.7, 418.3, 426.7, 451.8, 461.7, 466.8, 491.9, 500.8, 514.1, 543.6, 561.8, 593.6, 633.5, 674.3, 721.7, 789.7, 832.7, 885.5, 948.8, 1017.9, 1071.1, 1159.9, 1203.8],
[100.0, 99.0, 98.9, 102.3, 106.9, 108.7, 109.7, 108.4, 107.7, 111.8, 114.1, 115.7, 116.6, 115.7, 115.0, 120.1, 124.9, 127.4, 129.8, 129.6, 130.1, 142.8, 150.0, 153.4, 155.2, 154.3, 155.8, 166.0, 176.3, 182.2, 185.7, 186.3, 192.2, 210.5, 220.5, 230.2, 235.8, 238.3, 243.9, 254.1, 257.9, 277.4, 295.8, 305.5, 312.3, 330.5, 351.2, 364.1, 377.2, 382.4, 393.2, 435.7, 461.9, 471.6, 488.0, 494.0, 510.9, 541.7, 569.5, 592.8, 621.1, 636.0, 657.6, 729.0, 801.1, 847.2, 895.9, 971.8, 1067.5, 1181.0, 1261.6, 1318.3, 1371.3, 1402.8, 1457.8, 1595.1, 1768.1],
[100.0, 101.5, 107.0, 110.8, 117.4, 119.5, 121.6, 124.1, 126.8, 129.3, 130.4, 132.0, 155.6, 157.2, 163.2, 164.1, 177.2, 175.9, 180.6, 182.4, 193.7, 198.1, 215.5, 220.1, 226.7, 233.7, 248.7, 255.7, 263.2, 273.7, 281.1, 287.2, 293.3, 299.2, 304.8, 309.4, 316.0, 318.0, 320.0, 324.4, 324.4, 324.7, 327.7, 331.1, 338.8, 344.0, 352.0, 360.8, 371.5, 375.8, 383.4, 388.5, 401.9, 410.1, 420.3, 432.4, 437.4, 445.8, 457.0, 467.0, 476.8, 485.2, 498.9, 537.2, 561.7, 581.7, 621.3, 649.7, 685.7, 707.0, 759.8, 825.5, 860.1, 929.2, 973.8, 1037.2, 1095.5],
[100.0, 100.9, 101.3, 102.2, 103.3, 106.2, 107.5, 110.1, 111.2, 112.3, 113.1, 114.1, 117.4, 118.7, 120.7, 126.2, 127.8, 130.6, 135.8, 141.5, 146.0, 160.2, 167.0, 173.0, 176.2, 180.9, 185.9, 193.0, 201.9, 208.3, 215.5, 220.9, 234.3, 251.7, 272.2, 273.7, 288.5, 284.7, 290.7, 299.1, 302.8, 311.3, 324.0, 336.5, 348.2, 357.2, 373.2, 387.9, 397.2, 409.0, 427.6, 441.2, 460.1, 471.0, 486.2, 499.5, 515.9, 533.8, 548.5, 563.3, 582.3, 601.7, 628.3, 655.9, 692.0, 729.0, 772.6, 852.4, 924.2, 980.1, 1028.4, 1084.2, 1148.5, 1210.7, 1271.9, 1345.8, 1462.0],
[100.0, 102.4, 105.1, 107.1, 109.1, 110.7, 112.4, 116.1, 119.0, 121.9, 123.2, 124.8, 127.8, 130.0, 133.1, 134.8, 137.2, 140.3, 146.3, 150.4, 156.5, 163.6, 172.6, 182.3, 191.9, 197.5, 203.8, 210.4, 217.7, 228.8, 236.9, 246.7, 259.5, 281.1, 294.3, 312.8, 330.3, 323.7, 325.2, 333.8, 337.8, 341.3, 349.0, 356.5, 365.1, 377.9, 389.5, 403.8, 425.0, 439.4, 454.7, 472.9, 490.5, 513.8, 530.3, 550.5, 573.7, 598.4, 626.7, 641.9, 645.1, 671.7, 696.1, 730.6, 777.3, 825.6, 886.8, 947.5, 1001.7, 1045.1, 1119.4, 1165.3, 1231.5, 1291.8, 1360.5, 1438.3, 1533.0],
[100.0, 102.1, 104.0, 105.2, 105.9, 106.9, 107.6, 109.9, 111.1, 112.0, 113.4, 116.8, 120.6, 123.2, 128.8, 131.1, 136.3, 138.9, 147.2, 154.8, 161.1, 177.8, 191.4, 196.5, 201.2, 206.2, 210.7, 219.5, 229.1, 237.1, 240.8, 243.5, 253.1, 265.1, 274.4, 286.9, 301.2, 305.6, 310.6, 315.6, 319.9, 323.2, 329.1, 335.2, 344.7, 357.1, 372.0, 385.4, 404.3, 422.9, 443.3, 461.9, 488.0, 517.0, 533.9, 546.1, 559.5, 576.5, 594.3, 607.4, 637.3, 654.9, 687.2, 724.8, 763.5, 810.2, 848.6, 895.3, 956.5, 1012.1, 1057.3, 1121.7, 1186.6, 1257.1, 1318.2, 1388.2, 1477.7],
[100.0, 103.1, 107.3, 110.7, 118.3, 118.7, 120.1, 121.2, 123.1, 124.3, 130.9, 131.9, 134.1, 136.7, 149.1, 153.2, 154.8, 160.9, 161.5, 162.4, 182.6, 186.5, 187.8, 193.5, 208.3, 223.7, 226.1, 236.1, 244.3, 249.1, 266.9, 267.4, 270.6, 288.8, 290.1, 311.6, 341.4, 341.8, 349.8, 378.7, 363.1, 364.4, 365.9, 368.5, 369.6, 370.1, 369.6, 367.5, 367.4, 423.0, 430.7, 431.0, 433.3, 437.6, 468.4, 470.1, 467.3, 480.3, 485.7, 489.7, 498.8, 536.1, 544.0, 562.8, 583.3, 601.2, 603.6, 636.8, 662.6, 679.0, 761.3, 809.7, 837.2, 903.9, 974.8, 993.5, 1055.7],
[100.0, 103.2, 103.8, 105.5, 108.3, 109.1, 111.6, 115.6, 116.4, 119.5, 121.0, 121.9, 122.8, 127.0, 128.3, 129.8, 132.3, 135.6, 140.2, 147.3, 152.1, 162.5, 166.9, 171.6, 176.1, 182.3, 186.3, 190.0, 196.0, 200.7, 208.1, 216.2, 225.2, 242.2, 247.1, 255.3, 261.4, 274.5, 280.6, 287.6, 294.2, 301.4, 314.0, 324.3, 335.0, 341.3, 350.1, 367.9, 387.1, 405.7, 415.0, 437.2, 443.7, 457.5, 467.7, 482.2, 500.0, 518.9, 539.6, 547.5, 569.1, 593.1, 606.7, 626.9, 659.6, 693.9, 724.0, 819.5, 860.7, 905.8, 956.8, 996.7, 1042.7, 1136.1, 1205.9, 1258.8, 1353.7],
[100.0, 100.8, 104.0, 115.3, 118.7, 120.7, 121.9, 122.9, 125.3, 130.0, 131.0, 131.5, 131.5, 132.3, 134.8, 153.4, 154.7, 156.1, 158.0, 160.8, 163.9, 166.5, 170.0, 172.0, 173.7, 174.7, 177.6, 209.5, 212.7, 219.7, 223.5, 228.2, 233.9, 236.3, 240.1, 250.6, 255.6, 256.9, 260.6, 306.1, 301.6, 300.3, 301.5, 301.8, 304.1, 305.0, 305.4, 306.7, 306.9, 308.7, 309.0, 397.0, 406.9, 416.2, 420.9, 431.6, 449.9, 463.9, 470.4, 474.4, 479.1, 482.8, 495.6, 612.7, 635.5, 656.1, 669.4, 710.3, 745.7, 773.0, 827.9, 859.1, 892.6, 902.1, 930.5, 1201.1, 1261.5],
[100.0, 103.1, 104.9, 106.0, 108.0, 109.6, 110.9, 113.8, 114.6, 116.2, 117.8, 120.0, 122.1, 125.7, 128.3, 130.6, 133.6, 136.8, 140.4, 144.4, 147.9, 156.4, 161.3, 165.4, 169.8, 176.1, 182.5, 190.4, 198.2, 202.6, 207.7, 213.9, 221.6, 233.2, 239.1, 247.0, 255.1, 265.8, 273.9, 279.8, 284.0, 288.4, 294.9, 300.6, 306.2, 311.6, 322.1, 332.5, 347.7, 366.5, 386.3, 398.4, 413.9, 429.3, 442.7, 463.8, 477.4, 496.9, 517.4, 543.2, 575.1, 607.8, 634.0, 668.5, 717.6, 758.4, 805.5, 884.1, 942.9, 988.6, 1061.9, 1120.6, 1200.8, 1275.6, 1371.9, 1480.0, 1626.3],
[100.0, 101.9, 103.8, 105.7, 107.5, 108.9, 110.4, 111.8, 113.6, 115.5, 117.1, 118.5, 119.8, 122.7, 124.9, 127.2, 129.3, 131.9, 136.1, 141.4, 148.3, 160.1, 170.0, 177.5, 183.6, 190.3, 196.2, 202.2, 208.2, 214.0, 218.6, 224.5, 234.4, 253.7, 263.4, 276.3, 286.3, 295.1, 302.3, 308.4, 309.0, 314.9, 315.9, 323.3, 334.0, 340.1, 347.2, 356.4, 362.6, 370.0, 381.9, 390.3, 404.6, 416.3, 424.5, 438.1, 452.5, 462.3, 477.8, 487.6, 503.1, 524.7, 547.4, 577.7, 608.1, 635.8, 667.8, 721.9, 784.4, 837.7, 889.2, 941.2, 995.3, 1063.0, 1132.0, 1203.4, 1282.8]])

ipc_mean = np.mean(ipc, axis=0)

plt.clf()
plt.plot(ipc.T, alpha=0.5)
plt.plot(ipc_mean, "k", lw=3)
plt.ylabel("IPC-2016")

n_meses = len(meses)
dn = 3
plt.xticks(np.arange(0, n_meses, dn), meses[::dn], rotation=90)
plt.grid("on")
plt.tight_layout()

# %% fitear

# ==== convert dates to ordinal
from datetime import date

# create a dictionary with the months
month_dict = {}
for i, m in enumerate(["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]):
    month_dict[m] = i + 1


fechas = []
for me in meses:
    fe = date(2000 + int(me[-2:]), month_dict[me[:3]], day=1)
    fechas.append(fe)

def str2ord(me):
    fe = date(2000 + int(me[-2:]), month_dict[me[:3]], day=1)
    return fe.toordinal()

ordinals = np.array([str2ord(me) for me in meses])

# %% play with a few models
plt.clf()
plt.plot(ordinals, ipc_mean)

t0 = ordinals[0]
A0 = ipc_mean[0]
tau = (ordinals[-1] - ordinals[0]) / np.log(ipc_mean[-1] / ipc_mean[0])

from jax import jit, grad, jacfwd
import jax.numpy as jnp

@jit
def predictor(t, params):
    t0, A0, tau = params[:3]
    b = jnp.array(params[3:])

    t_norm = (t - t0) / tau
    powers = jnp.arange(b.shape[0]) + 1
    t_pows = t_norm.reshape((-1, 1))**powers.reshape((1, -1))
    t_poly = jnp.dot(t_pows, b)

    return A0 * jnp.exp(t_poly)

params0 = jnp.array([t0, A0, tau, 1.0, -0.03])
pred = predictor(ordinals, params0)
plt.plot(ordinals, pred)


@jit
def objective(params, ordinals, ipc_mean):
    pred = predictor(ordinals, params)
    er = ipc_mean - pred
    E = jnp.mean(er**2) # MSE

    return E

print(objective(params0, ordinals, ipc_mean))

from scipy.optimize import minimize

ret = minimize(objective, params0, args=(ordinals, ipc_mean), method='Nelder-Mead')
print(ret)


plt.plot(ordinals, predictor(ordinals, ret.x))

# progressively increase the order of the polinomial
paramsList = []
paramsList.append(ret.x)
fitErrList = []
fitErrList.append(ret.fun)

for i in range(24):
    print("======", i)
    param_last = paramsList[-1]
    params0 = jnp.array([*param_last, param_last[-1]])
    ret = minimize(objective, params0, args=(ordinals, ipc_mean), method='Nelder-Mead', tol=1e-9)

    paramsList.append(ret.x)
    fitErrList.append(ret.fun)
    print(ret)
    plt.plot(ordinals, predictor(ordinals, ret.x), label=i)


plt.legend()

plt.figure()
nPoly = [len(par) - 3 for par in paramsList]
plt.scatter(nPoly, fitErrList)

selected = nPoly.index(7)
PARAM_SCALE = np.copy(paramsList[selected]) # from now on, a global constant

print("selected paramaters scale:", PARAM_SCALE)

# %% re optimize to get final values and it's uncertainty

@jit
def predictor2(t, params):
    return predictor(t, params * PARAM_SCALE)

@jit
def objective2(params, ordinals, ipc_mean):
    pred = predictor2(ordinals, params)
    er = ipc_mean - pred
    E = jnp.mean(er**2) # MSE

    return E

params0 = np.random.normal(loc=np.ones_like(PARAM_SCALE), scale=1e-4)
print(predictor(ordinals, PARAM_SCALE))
print(predictor2(ordinals, params0))
print(objective2(params0, ordinals, ipc_mean))

ret = minimize(objective2, params0, args=(ordinals, ipc_mean), method='Powell', tol=1e-9)
print(ret)


plt.clf()
plt.plot(ordinals, ipc_mean)
plt.plot(ordinals, predictor2(ordinals, ret.x))

# mean and covariance of the parameters
params_mu = ret.x
# inverse of the negative hessian
# https://stats.stackexchange.com/questions/68080/basic-question-about-fisher-information-matrix-and-relationship-to-hessian-and-s?rq=1
obj_hess = jacfwd(grad(objective2))
params_cov = np.linalg.inv(obj_hess(params_mu, ordinals, ipc_mean))

pred2_jac = jacfwd(predictor2, 1)


@jit
def propagate2(ordinals, params_mu, params_cov):
    out_mu = predictor2(ordinals, params_mu)

    jacobians = pred2_jac(ordinals, params_mu)
    # n = ordinals[0]
    # out_cov = ordinals * 0.0
    # for i in jnp.arange(n):
    #     out_cov.at[i].set(jacobians[i].dot(params_cov).dot(jacobians[i]))
    out_cov = jnp.diag(jacobians.dot(params_cov).dot(jacobians.T))

    return out_mu, jnp.abs(out_cov)

pred_mu, pred_cov = propagate2(ordinals, params_mu, params_cov)



plt.clf()
plt.plot(ordinals, ipc_mean)
plt.errorbar(ordinals, pred_mu, yerr=3*np.sqrt(pred_cov))


# %% extrapolate to any other date

ord_test = np.array([str2ord(me) for me in ["Jul-22", "Jul-23"]])

pred_test, pred_cov = propagate2(ord_test, params_mu, params_cov)

price0 = 25130

price1 = price0 * pred_test[1] / pred_test[0]
price1_cov = price0 * pred_cov[1] / pred_test[0] + price0 * pred_test[1] / pred_test[0]**2 * pred_cov[0]
price1_std = np.sqrt(price1_cov)

print(price1, "+-", price1_std)
